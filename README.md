# The CNC by the sea

A Project about setting up a remote control for a CNC using CNCjs.

![](assets/main-capture.png)

This project shows how to control a CNC remotely using a raspberry pi, it can be used by students and fablabs to teach machine control and _potentially_manufacture with them remotely.

![](assets/diagram.png)

## How to replicate

Follow instructions below:

1. Flash the SD card with the raspbian version your preference (stretch in our case - **Recommended for mjpg_streamer easy compilation**)

2. Set it up for remote access, [best with ssh and without password authentification](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md#copy-your-public-key-to-your-raspberry-pi).

3. Install `cncjs` using the instructions [here](https://github.com/cncjs/cncjs/wiki/Setup-Guide:-Raspberry-Pi-%7C-System-Setup-&-Preparation). If you have already npm (maybe from node-red) installed in the pi, the setup is as simple as described in [the official docs]():

```
sudo npm install -g cncjs@latest --unsafe-perm
```

At this moment we should be able to run this:

```
cncjs --allow-remote-access
```

And see the interface in the local network (normally http://pi_ip_address:8000). Make sure your `.cncrc` file in your home directory is as shown in this repository.

You can now use PM2 to start cncjs automatically (another alternative is to use systemd - see below):

```
pm2 start "$(which cncjs) --allow-remote-access --port 8000"
pm2 startup systemd
sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u youruser --hp /home/youruser
pm2 save
pm2 list
```

And you should see:

```
...
0   │ cncjs    │ default     │ N/A     │ fork
...
```

Then to disable it

```
pm2 unstartup systemd
```

4. Setup the webcam stream following the instructions from [here](http://www.steinvoorte.nl/mjpgstreamer/index.php?page=2). There are some forks of the same project, and the patch provided in the link is the only one that runs flawlessly. The links below show alternatives of potentially (but not tested) similar outputs:
-  https://github.com/jacksonliam/mjpg-streamer
-  https://github.com/Motion-Project/motion/tree/master/doc

The streams can be made as a service using `systemd` in the pi. You can run as many streams as your pi supports (current example has two). The streams can be launched as shell scripts with the options in them or as services with an environment. 

**Bash script option**

```
cd /usr/local/bin
sudo nano run-mjpg-stream.sh
```

In it put:

```
mjpg_streamer -i "/usr/local/lib/input_uvc.so -d /dev/video0 -r 640x480 -f 15" -o "/usr/local/lib/output_http.so -p 8080 -w /usr/local/www"
```

Then `chmod`:

```
chmod 777 run-mjpg-stream.sh
```

Then the `service`.

```
cd /lib/systemd/system
sudo nano mjpg-stream.service
```

Then put in it:

```
[Unit]
Description=Streams video with Raspberry Pi Camera
After=syslog.target
After=network.target

[Service]
Type=simple
User=username
Group=username
WorkingDirectory=/home/username
ExecStart=/bin/bash /usr/local/bin/run-mjpg-stream.sh
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
```

Then start it:

```
sudo systemctl enable mjpg-stream.service
sudo systemctl start mjpg-stream.service
```

To disable it:

```
sudo systemctl stop mjpg-stream.service
sudo systemctl disable mjpg-stream.service
```

5. Make the pi accessible from outside your network. This can be either done using port forwarding on the router (and connecting to your router directly from the outside) or using a static IP address. The method show below is for the static IP address.

Set up an static IP address. This can be done with two methods `dhcpcd` and `networking`. Read [this link](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address/74428#74428) **very carefully** to understand the differences as the two methods coexist in current raspbian releases but the tutorials online don't always work. [This is a good read](https://raspberrypi.stackexchange.com/questions/37920/how-do-i-set-up-networking-wifi-static-ip-address/37921#37921) if you are using `dhcpcd`. The method hereby chosen is `networking` and the `/etc/network/interfaces` file is given in the assets (with the actual IP masked). Keep in mind that if you are using `dhcpcd` you have to modify `/etc/dhcpcd.conf` and that the submask is specified using [CIDR](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing). Read this and find the convertion of your submask in the table in the wikipedia page.

6. At this moment, you should see the static IP address in the pi when typing:

```
hostname -I
```

NB: a trick for getting your public IP address is also:

```
curl icanhazip.com
```

7. Setup an [nginx reverse proxy server](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/). The `cncjs` server will be running by default in http://localhost:8000 and the proxy will map an http (80) or https (443) port to the 8000 using this proxy. The documentation given [here](https://tinkerman.cat/post/secure-remote-access-to-your-iot-devices/) is perfect for setting this up, as well as the official documentation above. The nginx configuration in `/etc/nginx/sites-enabled/default` is also provided. This configuration includes access through 443 to:

- Access to the control application in `/`
- Access to the top camera stream in /stream-top/
- Capture a frame of the top camera in /capture-top/
- Access to the side camera stream in /stream-side/

It is very recommended that you use ssl and get certificates for encripting the connection (specially if using login authentification). The process is fully described in [tinkerman's site](https://tinkerman.cat/post/secure-remote-access-to-your-iot-devices/) and his documentation is really a threasure. Make sure that the files accessible in `/var/www/html` are being served (documentation [here](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/). You might need to use the default configuration first and then mount the provided one for the reverse proxy. 

For the SSL certificates we used `certbot` (https://certbot.eff.org/)

```
sudo apt-get install certbot
...
sudo certbot certonly --webroot -w /var/www/html -d thecncbythesea.hopto.org
```

NB: this could also be done with `iptables`, but it's recommended to use nginx or similar (apache) for increased security and performance.

Some useful commands for `nginx` debuggin:

```
# Test the current configuration
sudo nginx -t
# Reload the daemon
sudo systemctl reload nginx
```

9. At this moment we should have everything ready. Before deploying anything, you could check the following things:

- that the hostname (`hostname -I` is actually the static IP address)
- that when you run `cncjs --allow-remote-access` you can see the interface from the local network in port 8000
- that when you run the reverse proxy server, you can access the cncjs interface without specifying the port
- that the site is deployed with https only

10. You can set an user account at a server level (nginx level, following instructions here) or set them in cncjs by using the interface itself. This will be stored in `~/.cncrc`, with all passwords hashed.

## How to run it

1. Run the camera stream. First make sure that the camera is linux compatible:

```
lsusb
```

2. Start the stream or use the systemd service from above.

```
mjpg_streamer -i "/usr/local/lib/input_uvc.so -d /dev/video0 -r 640x480 -f 15" -o "/usr/local/lib/output_http.so -p 8080 -w /usr/local/www"
```

You should see it in the port `8080` of the ip address:

![](assets/stream-test.png)

3. Open `cncjs` and navigate to the ip or use the pm2 service from above:

```
cncjs --allow-remote-access
```

NB: you can configure the remote access option to be by default by changing `.cncrc` in your $HOME. Example `.cncrc` is also provided. The default is available [here](https://github.com/cncjs/cncjs/blob/master/.cncrc.default)

4. If you want you can remove some widgets and configure defaults. At this very moment this is not permanent, and has to be done for each new user/connection by loading a configuration json. An example json is provided and can be loaded/exported in `Settings`>`Workspaces`> (Settings is in the left-hand side, in the little gear's icon)

## Sand drawing Fabacademy 2020

We use this at fabacademy barcelona 2020 for drawing in the sadn with our students. Here are some results!

![](assets/hello-world.jpeg)

![](assets/mandala-finished.jpeg)

![](assets/mandala.jpg)



